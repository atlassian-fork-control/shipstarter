'use strict';

var galvatron = require('galvatron');
var gulp = require('gulp');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var karma = require('karma').server;
var mac = require('mac');

var browsers = ['Chrome'];
var reporters = ['progress'];

function run () {
    karma.start({
        hostname: '0.0.0.0',
        autoWatch: true,
        singleRun: false,
        frameworks: ['mocha', 'sinon-chai'],
        reporters: reporters,
        files: [
            'test/.tmp/unit.js'
        ]
    });
}

module.exports = mac.series(
    function () {
        return gulp.src('./test/unit.js')
            .pipe(galvatron.trace())
            .pipe(babel())
            .pipe(galvatron.globalize())
            .pipe(concat('unit.js'))
            .pipe(gulp.dest('test/.tmp'))
    },
    run
);