'use strict';

var distChippy = require('chippy/src/task/dist');
var mac = require('mac');
var gulp = require('gulp');

var distImages = function () {
  return gulp.src('./images/**')
    .pipe(gulp.dest('./public/images/'));
};

module.exports = mac.series(
  mac.parallel(
    distChippy,
    distImages
  )
);
