import * as jiraAPI from 'api/jira'
import * as projectsAPI from 'api/projects';
import Project from 'entities/project';

const issueKey = document.querySelector('meta[name="issue-key"]').getAttribute('content');
const container = document.querySelector('.shipment-order-converter-container');
const issueURL = '/rest/api/2/issue'
const convertButton = document.querySelector('.shipment-order-converter-button');
convertButton.addEventListener('click', function(){
    jiraAPI.callGet(`${issueURL}/${issueKey}`).then(issue => {
        projectsAPI.saveProject(new Project({
            description: issue.fields.description,
            lead: issue.fields.reporter.key,
            name: issue.fields.summary
        })).then((project) => {
                container.innerHTML = `
               <p> Your shipstarter project is ready, you can see it <a href="/atlassian-shipstarter/listing-page#!project/${project.data.key}" target="_blank">here</a></p>
               `
        });
    });

});