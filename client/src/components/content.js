'use strict';

import skate from 'skatejs';

export default skate('shipstarter-content', {
  template () {
    this.innerHTML = `
      <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
          <section class="aui-page-panel-content">
            ${this.innerHTML}
          </section>
        </div>
      </div>
    `;
  }
});
