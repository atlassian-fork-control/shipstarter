import * as topicsAPI from '../../api/topics';

function setupTopicsFilter(element) {
    topicsAPI.getAvailableTopics().then(topics => {
        const topicsFilter = element.querySelector('#topics-filter');
        const $topicsFilter = AJS.$(topicsFilter);
        topics.forEach(topic => {
            const newTopic = document.createElement('option');
            newTopic.textContent = topic;
            newTopic.value = topic;
            topicsFilter.appendChild(newTopic);
        });
        $topicsFilter.auiSelect2({
            containerCssClass: 'shipstarter-select2-topics-filter'
        });
        //can't use the delegate events because select2 fire jquery events which aren't caught by native listeners
        $topicsFilter.on('change', function(e) {
            skate.emit(topicsFilter, 'filter-change', {
                details: {
                    topics: $topicsFilter.value
                }
            });
        });
    });
}

export default skate('shipstarter-project-filter', {
    created: function () {
        setupTopicsFilter(this);
    },
    events: {
        'click': function (e) {
            var menu = document.getElementById(e.delegateTarget.getAttribute('aria-controls'));
            var selected = e.delegateTarget.querySelector('.shipstarter-dropdown-selected');
            menu.addEventListener('click', function handler(e) {
                selected.textContent = e.target.textContent.trim();
                this.removeEventListener('click', handler);
            });
        }
    },
    template() {
        this.innerHTML = `
    <form class="aui">
        <select id="topics-filter" multiple="">
        </select>
    </form>
    `;
    }
});