'use strict';

import 'whatwg-fetch';
import skate from 'skatejs';


function findInput(elem) {
    if (elem.type === 'text') {
        return elem.querySelector('input');
    } else {
        return elem.querySelector('textarea');
    }
}

function renderTextBox(elem, data) {
    var textContent = data;
    var TEXT_BOX_TEMPLATE = `<form class="aui"><input type="text" class="text" value="${textContent}"></form>`;
    elem.innerHTML = TEXT_BOX_TEMPLATE;
}

function renderTextArea(elem, data) {
    var textContent = data;
    var TEXTAREA_TEMPLATE = `<form class="aui"><textarea class="text">${textContent}</textarea></form>`;
    elem.innerHTML = TEXTAREA_TEMPLATE;
}

//we need to resize the textarea to match the height and width of the text inside
function resizeTextarea(elem) {
    var computedStyle = window.getComputedStyle(elem);
    var offsetHeight = elem.offsetHeight;
    var input = findInput(elem);

    if (elem.type !== 'text') {
        input.style.height = (offsetHeight + 10) + 'px';
        input.style.marginBottom = computedStyle['margin-bottom'];
        input.style.marginLeft = computedStyle['margin-left'];
        input.style.marginRight = computedStyle['margin-right'];
        input.style.marginTop = computedStyle['margin-top'];
        input.style.paddingBottom = computedStyle['padding-bottom'];
        input.style.paddingLeft = computedStyle['padding-left'];
        input.style.paddingRight = computedStyle['padding-right'];
        input.style.paddingTop = computedStyle['padding-top'];
    }
}

function enterEditMode(elem) {
    var textContent = elem.textContent.trim();

    if (elem.type === 'text') {
        renderTextBox(elem, textContent);
    } else {
        renderTextArea(elem, textContent);
    }
    var input = findInput(elem);
    resizeTextarea(elem);
    input.focus();
    input.select();
}

function exitEditMode(elem) {
    var input = findInput(elem);
    if (input) {
        elem.innerHTML = input.value.trim().split('\n').join('<br>');
    }
}

skate('shipstarter-inline-edit', {
    events: {
        click: function () {
            if(this.editable) {
                this.editing = true;
            }
        },
        'blur input, textarea': function () {
            if(this.editing) {
                this.editing = false;
                skate.emit(this, 'save', {
                    detail: {
                        name: this.name,
                        value: this.textContent
                    }
                });
            }
        },
        submit(e) {
            if(this.editing) {
                this.editing = false;
                skate.emit(this, 'save', {
                    detail: {
                        name: this.name,
                        value: this.textContent
                    }
                });
                e.preventDefault();
            }
        }
    },
    properties: {
        editable: {},
        editing: {
            attr: true,
            init: false,
            type: Boolean,
            set(value) {
                if (value) {
                    enterEditMode(this);
                } else {
                    exitEditMode(this);
                }
            }
        },
        name: {
            attr: true,
            type: String
        },
        type: {
            attr: true,
            type: String,
            init: 'text'
        }
    }
});
