//This module allows us to map data from a shipit project with projects as issues to our data model (projects as projects)

export function getIssuesInLatestShipitProject(projectKey) {
  var url = `/rest/api/2/search?jql=project=${projectKey}&maxResults=999`;
  return new Promise((resolve, reject) => {
    jira.callGet(url).then(function (data) {
      resolve(data);
    });
  });
}

//create projects from shipit project
export function createProjectsFromProjectIssues(projectKey) {
  var that = this;
  this.getIssuesInLatestShipitProject(projectKey).then(function (data) {
    var issues = data.issues;
    issues.forEach(function (issue) {
      that.saveProject(new Project({
        name: issue.fields.summary,
        description: issue.fields.description,
        lead: issue.fields.reporter.key
      }));
    });
  });
}
