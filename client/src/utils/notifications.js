export function saveProjectError(jiraErrors) {
  var errorList = '';
  for (var i in jiraErrors) {
    errorList += `<li>${jiraErrors[i]}</li>`;
  }

  AJS.flag({
    type: 'error',
    title: 'Sorry, we couldn\'t create your project.',
    close: 'auto',
    body: `<ul>${errorList}</ul>`
  });
}

export function createProjectSuccess(project) {
  success(`${project.name} created!`, 'Your project was added. You can keep adding details to it or start shipping right on this page!');
}

export function deleteProjectSuccess(project) {
    success('Project deleted', `"${project.name}" was deleted successfully`);
}

function success(title, message) {
  AJS.flag({
    type: 'success',
    title: title,
    close: 'auto',
    body: message
  });
}