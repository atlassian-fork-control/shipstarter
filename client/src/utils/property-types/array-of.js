'use strict';
//This module ensures that the type of a property is always an array of the desired type
export default function (type = String) {
  return function checkValue(value) {
    if (value === undefined) {
      return [];
    }
    //coerce the passed in value into an array if it isn't
    if (!Array.isArray(value)) {
      value = Array.prototype.slice.call(value);
    }

    //make sure each item in the passed in the value is of the correct type
    return value.map(item => {
      const isCorrectType = (item.constructor === type || item instanceof type);
      return  isCorrectType ? item : new type(item);
    });
  };
}
