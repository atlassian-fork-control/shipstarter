'use strict';

import BaseEntity from './baseEntity';
import _ from 'lodash';

var ENTITY_PROPERTIES = [
    'banner_url'
];

var PROPERTIES = [
    'key',
    'description',
    'lead',
    'name',
    'issueTypes',
    'projectTypeKey'
];

class Project extends BaseEntity {
    constructor(data={}) {
        super();

        if (!data.key) {
            data.key = 'S' + String(Date.now()).substr(-9, 9);
        }
        if (!data.name) {
            data.name = 'Choose your name';
        }
        this.set(data);
    }

    getEntityProperties() {
        return _.pick(this.data, ENTITY_PROPERTIES);
    }

    getProjectProperties() {
        return _.pick(this.data, PROPERTIES);
    }

}

export default Project;
