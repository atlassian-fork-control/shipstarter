import BaseEntity from './baseEntity';

class Task extends BaseEntity {
  constructor(task) {
    super();
    this.set(task);
  }

  static fromAPI(data) {
    return new Task({
        id: data.id,
        key: data.key,
        title: data.fields.summary,
        description: data.fields.description,
        projectId: data.fields.project.id
    });
  }

  toAPI(projectId) {
      return {
          project: {
              id: this.data.projectId || projectId
          },
          summary: this.data.title || undefined,
          description: this.data.description || undefined
      }
  }

}

export default Task