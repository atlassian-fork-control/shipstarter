class BaseEntity {

  constructor() {
    this.data = {};
  }

  get(key){
    if(!key){
      return this.data;
    }
    return this.data[key];
  }

  set(key, value){
    if(!value) {
      this.data = key;
    } else {
      this.data[key] = value;
    }
    
  }


}

export default BaseEntity;