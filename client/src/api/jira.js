const ISSUE_URL = '/rest/api/2/issue';
const SEARCH_URL = '/rest/api/2/search';

export function callGet(url) {
    return new Promise(function (resolve, reject) {
        AP.require('request', function (r) {
            r({
                url: url,
                success: function (data) {
                    resolve(JSON.parse(data));
                },
                error: function (e) {
                    reject(e);
                }
            });
        });
    });
}

export function callUpdate(url, data, type) {
    return new Promise(function (resolve, reject) {
        AP.require('request', function (r) {
            r({
                url: url,
                type: type || 'PUT',
                data: data,
                contentType: 'application/json',
                success: function (data) {
                    if(data) {
                        resolve(JSON.parse(data));
                    } else {
                        resolve();
                    }
                },
                error: function (response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    });
}

export function callDelete(url) {
    return new Promise(function (resolve, reject) {
        AP.require('request', function (r) {
            r({
                url: url,
                type: 'DELETE',
                contentType: 'application/json',
                success: function (data) {
                    resolve(data);
                },
                error: function (response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    });
}

export function jqlSearch(jql) {
    const url = `${SEARCH_URL}?jql=${encodeURI(jql)}`
    return callGet(url);
}

export function updateIssue(issueKeyOrId, updatedIssueData) {
    return callUpdate(`${ISSUE_URL}/${issueKeyOrId}`, JSON.stringify(updatedIssueData, 'PUT'));
};

export function createIssue(projectId, newIssueData) {
    newIssueData.fields = newIssueData.fields || {};

    newIssueData.fields.project = {
        id: projectId
    }
    return callUpdate(ISSUE_URL, JSON.stringify(newIssueData), 'POST');
};

export function getIssue(issueKeyOrId) {
    return callGet(`${ISSUE_URL}/${issueKeyOrId}`);
};

export function handleErrors(reject) {
    return function (errors) {
        reject(errors.errors);
    }
}
