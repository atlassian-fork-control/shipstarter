import * as jira from './jira';
import * as projectsApi from './projects';
import Task from '../entities/task';
import _ from 'lodash';


const PROJECTS_API_URL = '/rest/api/2/project';

function taskPledgeesUrl(taskId) {
    return `/rest/api/2/issue/${taskId}/properties/pledgees`;
}

function taskPledgeeDataUrl(taskId) {
    return `/rest/api/2/issue/${taskId}/properties/pledgeeData`;
}

//NOTE projectId != projectKey

function isIssueTypeTask(issueType) {
    return issueType.name === 'Task';
}

export function createTask(projectId, task) {
    var data = {
        fields: task.toAPI(projectId)
    };

    return jira.callGet(`${PROJECTS_API_URL}/${task.data.projectId}`).then(function (project) {
        const desiredIssueTypeId = _.find(project.issueTypes, isIssueTypeTask).id;
        data.fields.issuetype = {};
        data.fields.issuetype.id = desiredIssueTypeId;

        return jira.createIssue(projectId, data);
    });
}

export function updateTask(taskId, task) {
    var data = {
        fields: task.toAPI(task.projectId)
    }
    return jira.updateIssue(taskId, data);
}

export function getTask(taskId) {
    return jira.getIssue(taskId);
}

function convertToTask(issue) {
    return Task.fromAPI((issue));
}

function notMetaDataIssue(issue) {
    return !issue.fields.summary.match(projectsApi.METADATA_ISSUE_SUMMARY);
}

export function getTasksByProject(project_key, startAt = 0, maxResults = 9999999) {
    var jql = "project=" + project_key;
    var url = `/rest/api/2/search?jql=${encodeURIComponent(jql)}&startAt=${startAt}&maxResults=${maxResults}`;

    return jira.callGet(url).then(results => {
        let issues = results.issues.slice();
        return _.filter(issues, notMetaDataIssue).map(convertToTask);
    });
}

export function getTaskPledgees(taskId) {
    return jira.callGet(taskPledgeeDataUrl(taskId)).then(pledgeeData => {
        const pledgees = pledgeeData.value.pledgees;
        return pledgees;
    }).catch(error => {
        if(error.status === 404) {
            return [];
        }
    });
}

export function updatePledgeesForTask(taskId, newPledgees) {
    const updatePledgeeData = jira.callUpdate(taskPledgeeDataUrl(taskId), JSON.stringify({pledgees: newPledgees}), 'PUT');
    const updatePledgees = jira.callUpdate(taskPledgeesUrl(taskId), JSON.stringify({pledgees: _.pluck(newPledgees, "key")}), 'PUT');
    return Promise.all([updatePledgeeData, updatePledgees]).then(() => {
        return newPledgees;
    });
}

export function pledgeUserToTask(user, taskId) {
    return getTaskPledgees(taskId).then(pledgees => {
        const alreadyPledged = _.find(pledgees, value => value.key === user.key);
        if (!alreadyPledged) {
            const newPledgees = pledgees && pledgees.slice().concat([user]) || [user];
            return updatePledgeesForTask(taskId, newPledgees);
        } else {
            return pledgees
        }
    }).catch(error => {
        if (error.status === 404) {
            return updatePledgeesForTask(taskId, [user]);
        } else {
            return JSON.parse(error.responseText).errorMessages;
        }
    });
}

