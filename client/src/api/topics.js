import * as jira from './jira';
import * as projectsApi from './projects';

export function updateTopics(projectKeyOrId, updatedTopics) {
    //find existing meta issue
    return projectsApi.getMetaIssueKey(projectKeyOrId)
        .then((issueKey) => {
            return jira.updateIssue(issueKey, {
                fields: {
                    labels: updatedTopics
                }
            });
        });
}

export function getAvailableTopics () {
    return new Promise((resolve, reject) => {
        const result = [
            'Frontend',
            'Backend',
            'JIRA',
            'Confluence',
            'Service Desk',
            'Fusion',
            'Finance',
            'Talent',
            'Bitbucket',
            'Hipchat',
            'Foundation',
            'Cross-Product',
            'Engineering Services'
        ]
        resolve(result);
    });
}