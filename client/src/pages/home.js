'use strict';

import ProjectEntity from '../entities/project';
import ProjectList from '../components/project/list';
import ProjectFilter from '../components/project/project-filter';
import * as projectsApi from '../api/projects';
import Project from '../entities/project';
import arrayOf from '../utils/property-types/array-of'
import skate from 'skatejs';

export default skate('shipstarter-home-page', {
    properties: {
        events: {
            "filter-change": (e) => {
                this.getElementById('all-projects').filterByTopics(e.details.topics);
            }
        },
        projects: {
            type: arrayOf(ProjectEntity),
            set (projects) {
                this.querySelector('#all-projects').setProjects(projects);
            }
        },
        currentUser: {
            set (user) {
                const homePage = this;
                projectsApi.getProjectsByPledgees([user.key])
                    .then((projects) => {
                        const coercedProjects = _.map(projects, (projectData) => new Project(projectData))
                        homePage.querySelector('#your-projects').setProjects(coercedProjects);
                    });
            }
        }
    },
    template () {
        this.innerHTML = `
      <shipstarter-app>
        <shipstarter-banner class="banner-overview">
          <div class="titles">
            <h1><span class="logo">Atlassian</span> ShipStarter</h1>
            <h2>Ship just got real!</h2>
          </div>
          <div class="banner-actions">
            <a href="#/project" class="aui-button aui-button-primary create-project">Start a project</a>
          </div>
        </shipstarter-banner>
        <shipstarter-content>
          <div class="aui-group">
            <div class="aui-item shipstarter-breadcrumbs">
              <span>ShipStarter</span>
            </div>
          </div>
          <h2> Your projects </h2>
          <shipstarter-project-list id="your-projects" empty-state="You haven't pledged to any projects!"></shipstarter-project-list>
          <h2> Browse </h2>
          <shipstarter-project-filter></shipstarter-project-filter>
          <shipstarter-project-list id="all-projects" empty-state="There aren't any projects in shipstarter yet!"></shipstarter-project-list>
        </shipstarter-content>
      </shipstarter-app>
    `;
    }
});
